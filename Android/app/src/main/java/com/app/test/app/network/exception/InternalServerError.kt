package com.app.test.app.network.exception

import java.io.IOException

class InternalServerError : IOException()