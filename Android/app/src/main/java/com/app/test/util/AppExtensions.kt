package com.app.test.util

import android.databinding.BindingAdapter
import android.support.v4.widget.CircularProgressDrawable
import android.widget.ImageView
import com.app.test.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

// Binding adapter for imageview with glide
@BindingAdapter("android:src")
fun setImageView(imageView: ImageView, imageUrl:String) {
    val circularProgressDrawable = CircularProgressDrawable(imageView.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    Glide.with(imageView.context)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(circularProgressDrawable))
            .into(imageView)
}